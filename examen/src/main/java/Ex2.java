import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.Integer.parseInt;

public class Ex2 extends JFrame {
    JTextField text1, text2, text3;
    JButton buton1;

    Ex2() {
        setTitle("Exercitiul 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(250, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 100;
        int height = 40;

        text1 = new JTextField();
        text1.setBounds(60, 30, width, height);

        text2 = new JTextField();
        text2.setBounds(60, 80, width, height);

        text3 = new JTextField();
        text3.setBounds(60, 130, width, height);
        text3.setEditable(false);

        buton1 = new JButton("Calculeaza");
        buton1.setBounds(60, 180, width, height);

        buton1.addActionListener(new calculate());

        add(text3);
        add(text1);
        add(text2);
        add(buton1);
    }


    public static void main(String[] args) {
        new Ex2();
    }

    class calculate implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (Ex2.this.text1.getText().equals("") || Ex2.this.text2.getText().equals(""))
                Ex2.this.text3.setText("Camp gol!");
            else {
                try {
                    int a = parseInt(Ex2.this.text1.getText()); //conversie din String in int
                    int b = parseInt(Ex2.this.text2.getText());

                    String rez = Integer.toString(a + b);

                    Ex2.this.text3.setText(rez);
                } catch (NumberFormatException e1) {
                    Ex2.this.text3.setText("Doar numere intregi!");
                }
            }
        }

    }
}


