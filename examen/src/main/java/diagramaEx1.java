public class diagramaEx1 {

    class A {
        public void met(B b) {
            System.out.println(b); //dependenta
        }
    }

    class B {
        private long t;
        E myEobject; //asociere unidirectionala
        D d; //agregare

        B(D d) {
            this.d = d;
        }

        public void x() {

        }

    }

    interface C {

    }

    static class D {

    }

    class E implements C { //interfata C
        F fList = new F(); //compozitie

        public void metG(int i) {

        }
    }

    class F {
        public void metA() {

        }

    }
}
